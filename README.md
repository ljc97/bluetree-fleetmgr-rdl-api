Springboot Based API to handle retrieving Remote VU/Tacho downloads from BlueTree FleetManager

Uses selenium to access the platform and retrieve a list of downloads.

Stores downloads in a specified folder, and records history/details to a DB.