package com.arietis.bluetree.fleetmgr.rdl.utility;

public class Constants {
    public static final String FF_BROWSER_DOWNLOAD_FOLDER_LIST = "browser.download.folderList";
    public static final String FF_BROWSER_DOWNLOAD_DIR = "browser.download.dir";
    public static final String FF_BROWSER_HELPER_APPS_NEVER_ASK_SAVE_TO_DISK = "browser.helperApps.neverAsk.saveToDisk";
    public static final String ASP_NET_COOKIE = "ASP.NET_SessionId";
}
