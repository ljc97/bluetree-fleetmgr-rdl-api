package com.arietis.bluetree.fleetmgr.rdl.service;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.repository.RDLDownloadRepository;
import com.arietis.bluetree.fleetmgr.rdl.scraper.FleetMgrRdlScraper;
import com.arietis.bluetree.fleetmgr.rdl.scraper.FleetMgrScraper;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ScanService {

    @NonNull
    private ScraperService scraperService;
    @NonNull
    private Helper helper;
    @NonNull
    private DownloadService downloadService;
    @NonNull
    private RDLDownloadRepository rdlDownloadRepository;
    @NonNull
    private Configuration configuration;
    @NonNull
    private FleetMgrScraper fleetMgrScraper;
    @NonNull
    private FleetMgrRdlScraper fleetMgrRdlScraper;

    public void scanAndDownloadPeriod(Date startDate, Date endDate) {
        if (endDate == null) {
            endDate = startDate;
        }
        WebDriver webDriver = helper.createFirefoxDriver();
        log.info("Retrieving allDownloadsForDay from {} to {}", startDate, endDate);
        List<RDLDownload> allDownloadsForPeriod = scraperService.retrieveDownloads(webDriver, startDate, endDate);
        log.info("Sorting downloads to be downloaded");
        List<RDLDownload> toBeDownloaded = scraperService.resolveRemainingDownloads(allDownloadsForPeriod);
        for (RDLDownload toDownload : toBeDownloaded) {
            try {
                Boolean downloadStatus = downloadService.downloadFile(webDriver, toDownload);
                if (downloadStatus) {
                    log.info("Download completed successfully, updating repository");
                }
                toDownload.setRetrievalComplete(downloadStatus);
                toDownload.setRetrievalDate(new Date());
                rdlDownloadRepository.save(toDownload);
            } catch (Exception exc) {
                log.error("Error dealing with download", exc);
            }

        }
        webDriver.close();
    }

    public void runCatchup(Date startDate, Date endDate) {
        if (endDate == null) {
            endDate = new Date();
        }
        log.info("Running catchup from date {} to date {}", startDate, endDate);
        scanAndDownloadPeriod(startDate, endDate);
    }

    public void runMissingDownloads() {
        log.info("Downloading Missing files");

        // Correct any null status downloads
        List<RDLDownload> nullDownloads = rdlDownloadRepository.findAllByRetrievalComplete(null);
        for (RDLDownload nullDownload : nullDownloads) {
            nullDownload.setRetrievalComplete(false);
            rdlDownloadRepository.save(nullDownload);
        }

        List<RDLDownload> incompleteDownloads = rdlDownloadRepository.findAllByRetrievalComplete(false);
        if (incompleteDownloads.size() > 0) {
            log.info("Have {} files to download", incompleteDownloads.size());
            WebDriver webDriver = helper.createFirefoxDriver();
            fleetMgrScraper.login(webDriver, configuration.getFleetMgrAccount());
            fleetMgrRdlScraper.navigateToDownloadPage(webDriver);

            for (RDLDownload toDownload : incompleteDownloads) {
                Boolean downloadStatus = downloadService.downloadFile(webDriver, toDownload);
                toDownload.setRetrievalComplete(downloadStatus);
                toDownload.setRetrievalDate(new Date());
                rdlDownloadRepository.save(toDownload);
            }

            webDriver.close();
        } else {
            log.info("No downloads missing");
        }
    }

    public List<RDLDownload> listDownloads() {
        List<RDLDownload> downloads = new ArrayList<>();
        rdlDownloadRepository.findAll().forEach(downloads::add);
        return downloads;
    }
}
