package com.arietis.bluetree.fleetmgr.rdl.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RDLDownload {

    private String dlc;
    private String ddd;
    private String clcALC;
    private String type;
    @Id
    @Column(name = "filename", unique = true, nullable = false)
    private String filename;
    private Date activitiesFrom;
    private Date activitiesTo;
    private Date downloadDate;
    private String fromWhere;
    private String how;
    private Boolean retrievalComplete;
    private Date retrievalDate;
}
