package com.arietis.bluetree.fleetmgr.rdl.controller;

import com.arietis.bluetree.fleetmgr.rdl.dto.CatchupRequest;
import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.service.ScanService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ManagementController {

    @NonNull
    private ScanService scanService;

    @PostMapping(value = "/catchup")
    public void runCatchup(@RequestBody CatchupRequest catchupRequest) {
        if (catchupRequest.getStartDate() == null) {
            return;
        }
        scanService.runCatchup(catchupRequest.getStartDate(), catchupRequest.getEndDate());
    }

    @GetMapping(value = "/today")
    public void scanToday() {
        scanService.scanAndDownloadPeriod(new Date(), null);
    }

    @GetMapping(value = "/downloadMissing")
    public void downloadMissing() {
        scanService.runMissingDownloads();
    }

    @GetMapping(value = "/downloads")
    public List<RDLDownload> viewDownloads() {
        return scanService.listDownloads();
    }


}
