package com.arietis.bluetree.fleetmgr.rdl.entity;

import lombok.Data;

@Data
public class FleetMgrAccount {

    private String username;
    private String password;
    private String organization;
}
