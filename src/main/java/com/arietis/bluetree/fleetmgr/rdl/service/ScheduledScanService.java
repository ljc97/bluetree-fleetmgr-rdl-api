package com.arietis.bluetree.fleetmgr.rdl.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
@RequiredArgsConstructor
public class ScheduledScanService {

    @NonNull
    private ScanService scanService;

    @Scheduled(fixedDelayString = "1800000")
    public void scheduledScanOfToday() {
        log.info("Running scheduled scan of today");
        scanService.scanAndDownloadPeriod(new Date(), null);
        log.info("Finished scheduled scan");
    }
}
