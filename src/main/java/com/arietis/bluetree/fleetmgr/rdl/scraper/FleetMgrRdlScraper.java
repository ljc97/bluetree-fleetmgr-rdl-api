package com.arietis.bluetree.fleetmgr.rdl.scraper;

import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class FleetMgrRdlScraper {

    @NonNull
    private Helper helper;

    public Boolean navigateToDownloadPage(WebDriver webDriver) {
        try {
            webDriver.get("https://bluetreesystems.tachoweb.eu/management/masterdata.company");
            helper.sleepForTime(5000L);

            webDriver.get("https://bluetreesystems.tachoweb.eu/download/download.search");
            helper.sleepForTime(5000L);

            webDriver.findElement(By.id("Content_TextBox_SearchFromDriver"));
            webDriver.findElement(By.id("Content_TextBox_SearchToDriver"));
            return true;
        } catch (Exception exc) {
            log.error("Error navigating to RDL page", exc);
            return false;
        }
    }

    // method to scrape downloads for a particular Day, using an already logged in session
    public List<RDLDownload> retrieveDownloadsForPeriod(WebDriver webDriver, Date startDate, Date endDate) {
        try {
            WebElement driverOverview = webDriver.findElement(By.id("OverviewDriver"));
            WebElement byDownloadButton = driverOverview.findElement(By.id("RadioDownloadPeriodDriver"));
            WebElement fromDateField = driverOverview.findElement(By.id("Content_TextBox_SearchFromDriver"));
            WebElement toDateField = driverOverview.findElement(By.id("Content_TextBox_SearchToDriver"));

            SimpleDateFormat dateRangeFormat = new SimpleDateFormat("dd/MM/yyyy");

            byDownloadButton.click();
            // set fromDate
            fromDateField.sendKeys(Keys.CONTROL + "a");
            fromDateField.sendKeys(Keys.DELETE);
            fromDateField.sendKeys(dateRangeFormat.format(startDate));
            // set toDate
            toDateField.sendKeys(Keys.CONTROL + "a");
            toDateField.sendKeys(Keys.DELETE);
            toDateField.sendKeys(dateRangeFormat.format(endDate));

//            String after = toDateField.getText();
//            after = fromDateField.getText();
            WebElement submitButton = webDriver.findElement(By.id("Content_buttonShowResultDriver"));
            submitButton.click();
            helper.sleepForTime(5000L);

            WebElement dataTable = webDriver.findElement(By.xpath("//*[@aria-describedby='TableResultDriver_info']"));

            List<RDLDownload> rdlDownloads = new ArrayList<>();
            List<WebElement> tableRows = dataTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
            log.info("Found {} rows of data for {} to {}", tableRows.size(), startDate, endDate);
            if (tableRows.size() == 1 && tableRows.get(0).getAttribute("innerHTML").contains("No records")) {
                log.info("No records found for current period");
                return rdlDownloads;
            }
            for (WebElement row : tableRows) {
                List<WebElement> columns = row.findElements(By.tagName("td"));
                WebElement dlcElement = columns.get(0);
                WebElement dddElement = columns.get(1);
//                WebElement clcAlcElement = columns.get(2);
                WebElement typeElement = columns.get(3);
                WebElement fileNameElement = columns.get(4);
                WebElement activitiesFromElement = columns.get(5);
                WebElement activitiesToElement = columns.get(6);
                WebElement downloadDateElement = columns.get(7);
                WebElement whereElement = columns.get(8);
                WebElement howElement = columns.get(9);
                RDLDownload rdlDownload = new RDLDownload();
                rdlDownload.setDlc(dlcElement.findElement(By.tagName("a")).getAttribute("href"));
                rdlDownload.setDdd(dddElement.findElement(By.tagName("a")).getAttribute("href"));
                rdlDownload.setClcALC(null);
                rdlDownload.setType(typeElement.getText());
                rdlDownload.setFilename(fileNameElement.getText());
                // date fmt 16.09.2019 01:00
                SimpleDateFormat activityDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
                rdlDownload.setActivitiesFrom(activityDateFormat.parse(activitiesFromElement.getText()));
                rdlDownload.setActivitiesTo(activityDateFormat.parse(activitiesToElement.getText()));
                rdlDownload.setDownloadDate(activityDateFormat.parse(downloadDateElement.getText()));
                rdlDownload.setFromWhere(whereElement.getText());
                rdlDownload.setHow(howElement.getText());
                rdlDownloads.add(rdlDownload);
            }
            log.info("Recorded {} rdlDownloads", rdlDownloads.size());
            return rdlDownloads;
        } catch (Exception exc) {
            log.error("Issue encountered retrieving daily downloads", exc);
        }
        return null;
    }

}
