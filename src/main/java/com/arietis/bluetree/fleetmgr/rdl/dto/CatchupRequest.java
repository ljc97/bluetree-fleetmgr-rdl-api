package com.arietis.bluetree.fleetmgr.rdl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatchupRequest {
    @NonNull
    private Date startDate;
    private Date endDate;
}
