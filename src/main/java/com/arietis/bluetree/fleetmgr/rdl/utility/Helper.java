package com.arietis.bluetree.fleetmgr.rdl.utility;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Component
@RequiredArgsConstructor
public class Helper {

    @NonNull
    private Configuration configuration;

    public void sleepForTime(Long time) {
        log.debug("Sleeping for {} ms", time);
        try {
            Thread.sleep(time);
        } catch (Exception exc) {
            log.error("Unable to sleep thread for {} ms", time);
        }
    }


    /**
     * Create a pre-configured firefox driver
     * Uses configuration properties (headlessMode, downloadDirectory) to setup a sensible firefox driver configuration
     * Requires geckodriver etc to be available
     *
     * @return the configured firefox driver as a web driver
     */
    public WebDriver createFirefoxDriver() {
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        if (configuration.isHeadlessMode()) {
            log.debug("Setting Headless Mode");
            firefoxBinary.addCommandLineOptions("--headless");
        }
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setBinary(firefoxBinary);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference(Constants.FF_BROWSER_DOWNLOAD_FOLDER_LIST, 2);
        firefoxProfile.setPreference(Constants.FF_BROWSER_DOWNLOAD_DIR, configuration.getDownloadDirectory());
        firefoxProfile.setPreference(Constants.FF_BROWSER_HELPER_APPS_NEVER_ASK_SAVE_TO_DISK, MediaType.APPLICATION_OCTET_STREAM_VALUE);
        firefoxOptions.setProfile(firefoxProfile);
        return createFirefoxDriver(firefoxOptions);
    }


    /**
     * Method to create firefox driver from firefox options
     * Mainly used for testing
     *
     * @param firefoxOptions the firefox options to use
     * @return the firefox driver as a web driver
     */
    public WebDriver createFirefoxDriver(FirefoxOptions firefoxOptions) {
        return new FirefoxDriver(firefoxOptions);
    }

    /**
     * Close all tabs not matching the original window handle
     * Enumerates all tabs and then closes in turn
     * Switches back to main tab before returning
     *
     * @param webDriver            the web driver to operate on
     * @param originalWindowHandle the original window handle
     */
    public void closeAdditionalTabs(WebDriver webDriver, String originalWindowHandle) {
        log.info("Closing additional tabs");
        log.debug("Original Handle {}", originalWindowHandle);
        for (String windowHandle : webDriver.getWindowHandles()) {
            log.debug("Current WindowHandle {}", windowHandle);
            if (!originalWindowHandle.equals(windowHandle)) {
                webDriver.switchTo().window(windowHandle).close();
                log.debug("Closed handle {}", windowHandle);
            }
        }
        webDriver.switchTo().window(originalWindowHandle);
    }

    public void moveFile(Path fromPath, Path toPath) throws IOException {
        try {
            Files.move(fromPath, toPath);
        } catch (FileAlreadyExistsException exc) {
            log.error("Error moving temp file, dest already exits from {} to {}, deleting temp file", fromPath, toPath);
            Files.delete(fromPath);
            throw exc;
        }

    }
}
