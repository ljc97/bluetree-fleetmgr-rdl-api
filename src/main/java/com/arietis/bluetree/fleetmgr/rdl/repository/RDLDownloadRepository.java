package com.arietis.bluetree.fleetmgr.rdl.repository;

import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RDLDownloadRepository extends CrudRepository<RDLDownload, String> {

    @Override
    Optional<RDLDownload> findById(String s);

    @Override
    <S extends RDLDownload> S save(S entity);

    @Override
    boolean existsById(String s);

    List<RDLDownload> findAllByRetrievalComplete(Boolean retrievalComplete);
}
