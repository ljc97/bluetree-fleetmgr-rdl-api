package com.arietis.bluetree.fleetmgr.rdl.service;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.utility.Constants;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.StreamUtils;
import org.openqa.selenium.WebDriver;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;

@Service
@Slf4j
@RequiredArgsConstructor
public class DownloadService {

    @NonNull
    private Configuration configuration;
    @NonNull
    private RestTemplate restTemplate;
    @NonNull
    private Helper helper;

    /**
     * Downloads file using an active session and an RDL Download record
     * Uses ASP.NET Session Cookie from provided Web Driver
     * Prefers download DDD file first, then DLC file second
     *
     * @param webDriver   the web driver with a current, authenticated session
     * @param rdlDownload the rdl record to be downloaded
     * @return the completion status (true/false)
     */
    public Boolean downloadFile(WebDriver webDriver, RDLDownload rdlDownload) {

        String sessionID = webDriver.manage().getCookieNamed(Constants.ASP_NET_COOKIE).getValue();
        String fileName = rdlDownload.getFilename().substring(0, rdlDownload.getFilename().lastIndexOf("."));
        try {
            log.info("Attempting to download file {} (DDD)", fileName);
            attemptDownload(sessionID, rdlDownload.getDdd(), fileName, ".ddd");
            return true;
        } catch (Exception exc) {
            log.warn("Unable to download DDD file", exc);
        }
        try {
            log.info("Attempting to download file {} (DLC)", fileName);
            attemptDownload(sessionID, rdlDownload.getDlc(), fileName, ".dlc");
            return true;
        } catch (Exception exc) {
            log.warn("Unable to download DLC file", exc);
        }
        log.error("Unable to download any file for {}", rdlDownload.getFilename());
        return false;
    }

    private void attemptDownload(String sessionID, String url, String fileName, String extension) throws Exception {
        File temporaryFile = restTemplate.execute(url, HttpMethod.GET, clientHttpRequest ->
                clientHttpRequest.getHeaders().add("Cookie", Constants.ASP_NET_COOKIE + "=" + sessionID), clientHttpResponse -> {
            File tempFile = File.createTempFile(fileName, "tmp", new File(configuration.getDownloadDirectory()));
            FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
            StreamUtils.copy(clientHttpResponse.getBody(), fileOutputStream);
            fileOutputStream.close();
            return tempFile;
        });
        log.info("Download complete, moving temporary file to final");
        Path fromPath = temporaryFile.toPath();
        String newName = fileName + extension;
        Path toPath = Path.of(configuration.getDownloadDirectory(), newName);
        log.debug("Moving from {} to {}", fromPath, toPath);
        helper.moveFile(fromPath, toPath);
        log.info("Completed move of file, returning");
    }
}
