package com.arietis.bluetree.fleetmgr.rdl.config;

import com.arietis.bluetree.fleetmgr.rdl.entity.FleetMgrAccount;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties("app")
@Data
public class Configuration {

    private FleetMgrAccount fleetMgrAccount;
    private boolean headlessMode;
    private String downloadDirectory;
    private List<String> apiKeys;
    private String chromedriverPath;
    private String geckodriverPath;

}
