package com.arietis.bluetree.fleetmgr.rdl.service;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.repository.RDLDownloadRepository;
import com.arietis.bluetree.fleetmgr.rdl.scraper.FleetMgrRdlScraper;
import com.arietis.bluetree.fleetmgr.rdl.scraper.FleetMgrScraper;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ScraperService {
    @NonNull
    private FleetMgrScraper fleetMgrScraper;
    @NonNull
    private Configuration configuration;
    @NonNull
    private Helper helper;
    @NonNull
    private FleetMgrRdlScraper fleetMgrRdlScraper;
    @NonNull
    private RDLDownloadRepository rdlDownloadRepository;

    // for a particular date, return a list of all items found
    public List<RDLDownload> retrieveDownloads(WebDriver webDriver, Date startDate, Date endDate) {
        Boolean loginStatus = fleetMgrScraper.login(webDriver, configuration.getFleetMgrAccount());
        log.info("Login Status {}", loginStatus);
        List<RDLDownload> rdlDownloads = new ArrayList<>();
        if (loginStatus) {
            log.info("Navigate to RDL page");
            Boolean rdlStatus = fleetMgrRdlScraper.navigateToDownloadPage(webDriver);
            log.info("RDL Status {}", rdlStatus);

            log.info("Retrieve downloads for today");
            rdlDownloads = fleetMgrRdlScraper.retrieveDownloadsForPeriod(webDriver, startDate, endDate);
        }

        log.info("Completed retrieveDownloads from {} to {}", startDate, endDate);

        return rdlDownloads;
    }


    public List<RDLDownload> resolveRemainingDownloads(List<RDLDownload> rdlDownloads) {
        List<RDLDownload> toDownload = new ArrayList<>();

        rdlDownloads.parallelStream().forEach(rdlDownload -> {
            if (rdlDownloadRepository.existsById(rdlDownload.getFilename())) {
                log.info("Found file already existing in DB {}", rdlDownload.getFilename());
                RDLDownload fromRepository = rdlDownloadRepository.findById(rdlDownload.getFilename()).get();
                if (fromRepository.getRetrievalComplete() != null && fromRepository.getRetrievalComplete()) {
                    log.info("Download already complete, nothing to do");
                } else {
                    toDownload.add(rdlDownload);
                }
            } else {
                log.info("Saving new file record to DB {}", rdlDownload.getFilename());
                rdlDownloadRepository.save(rdlDownload);
                toDownload.add(rdlDownload);
            }


        });
        return toDownload;
    }

}
