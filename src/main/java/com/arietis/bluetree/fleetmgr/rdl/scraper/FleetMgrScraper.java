package com.arietis.bluetree.fleetmgr.rdl.scraper;

import com.arietis.bluetree.fleetmgr.rdl.entity.FleetMgrAccount;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class FleetMgrScraper {

    @NonNull
    private Helper helper;

    //method to login to bluetree fleetmgr
    public Boolean login(WebDriver webDriver, FleetMgrAccount fleetMgrAccount) {
        try {
            webDriver.get("https://www.fleetmanager.com/#/login");
            helper.sleepForTime(5000L);

            log.info("Locating Login Form Elements");
            WebElement loginForm = webDriver.findElement(By.className("form-signin"));
            log.debug("LoginForm HTML -> {}", loginForm.getAttribute("innerHTML"));
            WebElement usernameField = loginForm.findElement(By.xpath("//*[@placeholder='Username']"));
            WebElement passwordField = loginForm.findElement(By.xpath("//*[@placeholder='Password']"));
            WebElement organizationField = null;
            try {
                organizationField = loginForm.findElement(By.xpath("//*[@placeholder='Organisation']"));
            } catch (Exception exc) {
                log.debug("Unable to find Organisation field with S, trying Z");
            }
            if (organizationField == null) {
                organizationField = loginForm.findElement(By.xpath("//*[@placeholder='Organization']"));
            }

            log.info("Submitting login information");
            usernameField.sendKeys(fleetMgrAccount.getUsername());
            passwordField.sendKeys(fleetMgrAccount.getPassword());
            organizationField.sendKeys(fleetMgrAccount.getOrganization());

            WebElement submitButton = loginForm.findElement(By.xpath("//*[@type='submit']"));
            helper.sleepForTime(500L);

            log.info("Logging in");
            submitButton.click();

            try {
                helper.sleepForTime(3000L);
                String originalWindowHandle = webDriver.getWindowHandle();
                log.debug("Original Window Handle {}", originalWindowHandle);
                WebElement workingHoursAnalysis = webDriver.findElement(By.className("workingHoursAnalysis"));
                workingHoursAnalysis.click();
                helper.sleepForTime(2000L);

                helper.closeAdditionalTabs(webDriver, originalWindowHandle);

                log.info("Successfully logged in");
                return true;
            } catch (Exception exc) {
                log.error("Unable to locate expected elmements after login, check creds manually?", exc);
                return false;
            }
        } catch (Exception exc) {
            log.error("Unable to login to FleetMgr", exc);
            return false;
        }
    }
}
