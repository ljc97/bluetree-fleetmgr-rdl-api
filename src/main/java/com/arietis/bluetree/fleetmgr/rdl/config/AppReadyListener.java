package com.arietis.bluetree.fleetmgr.rdl.config;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class AppReadyListener implements ApplicationListener<ApplicationReadyEvent> {

    @NonNull
    private Configuration configuration;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if (configuration.getChromedriverPath() == null && configuration.getGeckodriverPath() == null) {
            log.error("No Drivers configured for Selenium");
            System.exit(1);
        }
        if (configuration.getChromedriverPath() != null) {
            log.info("Setting Chrome Driver Path " + configuration.getChromedriverPath());
            System.setProperty("webdriver.chrome.driver", configuration.getChromedriverPath());
        } else {
            log.warn("Not configuring Chrome Driver Path");
        }
        if (configuration.getChromedriverPath() != null) {
            log.info("Setting Gecko Driver Path " + configuration.getGeckodriverPath());
            System.setProperty("webdriver.gecko.driver", configuration.getGeckodriverPath());
        } else {
            log.warn("Not configuring Gecko Driver Path");
        }


    }
}
