package com.arietis.bluetree.fleetmgr.rdl.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@ConditionalOnProperty(
        value = "app.scheduled.tasks.enable", havingValue = "true"
)
@Configuration
@EnableScheduling
public class ScheduleConfiguration {

}
