package com.arietis.bluetree.fleetmgr.rdl.utility;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import com.google.common.collect.Sets;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatNullPointerException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelperTest {

    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();
    Configuration configuration;
    Helper helper;

    @Before
    public void setup() {
        configuration = Mockito.mock(Configuration.class);
        helper = Mockito.spy(new Helper(configuration));
    }

    @Test
    public void test_createFirefoxDriver_headless_addsHeadless() {
        Mockito.when(configuration.isHeadlessMode()).thenReturn(true);
        Mockito.when(configuration.getDownloadDirectory()).thenReturn("downloads");
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        ArgumentCaptor<FirefoxOptions> firefoxOptionsArgumentCaptor = ArgumentCaptor.forClass(FirefoxOptions.class);
        Mockito.doReturn(mockWebDriver).when(helper).createFirefoxDriver(firefoxOptionsArgumentCaptor.capture());

        WebDriver createdWebDriver = helper.createFirefoxDriver();

        softly.assertThat(createdWebDriver).isEqualTo(mockWebDriver);
        FirefoxOptions passedOptions = firefoxOptionsArgumentCaptor.getValue();
        softly.assertThat(passedOptions.getBinary()).isNotNull();

        // Check Headless Mode
        FirefoxBinary firefoxBinary = passedOptions.getBinary();
        try {
            Field extraOptionsField = firefoxBinary.getClass().getDeclaredField("extraOptions");
            extraOptionsField.setAccessible(true);
            List<String> extraOptions = (List<String>) extraOptionsField.get(firefoxBinary);
            softly.assertThat(extraOptions).isNotNull();
            softly.assertThat(extraOptions).contains("--headless");
            extraOptionsField.setAccessible(false);

        } catch (Exception exc) {
            softly.fail("Unable to reflectively check extraOptions", exc);
        }
        // Check Profile Options
        softly.assertThat(passedOptions.getProfile()).isNotNull();
        FirefoxProfile passedProfile = passedOptions.getProfile();
        softly.assertThat(passedProfile.getStringPreference(Constants.FF_BROWSER_DOWNLOAD_DIR, null)).isEqualTo("downloads");
        softly.assertThat(passedProfile.getIntegerPreference(Constants.FF_BROWSER_DOWNLOAD_FOLDER_LIST, -1)).isEqualTo(2);
        softly.assertThat(passedProfile.getStringPreference(Constants.FF_BROWSER_HELPER_APPS_NEVER_ASK_SAVE_TO_DISK, null)).isEqualTo(MediaType.APPLICATION_OCTET_STREAM_VALUE);

    }

    @Test
    public void test_createFirefoxDriver_notHeadless_doesntAddHeadless() {
        Mockito.when(configuration.isHeadlessMode()).thenReturn(false);
        Mockito.when(configuration.getDownloadDirectory()).thenReturn("downloads");
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        ArgumentCaptor<FirefoxOptions> firefoxOptionsArgumentCaptor = ArgumentCaptor.forClass(FirefoxOptions.class);
        Mockito.doReturn(mockWebDriver).when(helper).createFirefoxDriver(firefoxOptionsArgumentCaptor.capture());

        WebDriver createdWebDriver = helper.createFirefoxDriver();

        softly.assertThat(createdWebDriver).isEqualTo(mockWebDriver);
        FirefoxOptions passedOptions = firefoxOptionsArgumentCaptor.getValue();
        softly.assertThat(passedOptions.getBinary()).isNotNull();
        // Check Headless Mode
        FirefoxBinary firefoxBinary = passedOptions.getBinary();
        try {
            Field extraOptionsField = firefoxBinary.getClass().getDeclaredField("extraOptions");
            extraOptionsField.setAccessible(true);
            List<String> extraOptions = (List<String>) extraOptionsField.get(firefoxBinary);
            softly.assertThat(extraOptions).isNotNull();
            softly.assertThat(extraOptions).doesNotContain("--headless");
            extraOptionsField.setAccessible(false);

        } catch (Exception exc) {
            softly.fail("Unable to reflectively check extraOptions", exc);
        }
        // Check Profile Options
        softly.assertThat(passedOptions.getProfile()).isNotNull();
        FirefoxProfile passedProfile = passedOptions.getProfile();
        softly.assertThat(passedProfile.getStringPreference(Constants.FF_BROWSER_DOWNLOAD_DIR, null)).isEqualTo("downloads");
        softly.assertThat(passedProfile.getIntegerPreference(Constants.FF_BROWSER_DOWNLOAD_FOLDER_LIST, -1)).isEqualTo(2);
        softly.assertThat(passedProfile.getStringPreference(Constants.FF_BROWSER_HELPER_APPS_NEVER_ASK_SAVE_TO_DISK, null)).isEqualTo(MediaType.APPLICATION_OCTET_STREAM_VALUE);
    }

    @Test
    public void test_createFirefoxDriver_nullDownloads_throwsException() {
        Mockito.when(configuration.isHeadlessMode()).thenReturn(false);
        Mockito.when(configuration.getDownloadDirectory()).thenReturn(null);
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        ArgumentCaptor<FirefoxOptions> firefoxOptionsArgumentCaptor = ArgumentCaptor.forClass(FirefoxOptions.class);
        Mockito.doReturn(mockWebDriver).when(helper).createFirefoxDriver(firefoxOptionsArgumentCaptor.capture());

        assertThatNullPointerException().isThrownBy(() -> helper.createFirefoxDriver());
    }

    @Test
    public void test_closeWindowHandles_oneWindow_doesNotCallClose() {
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        Set<String> allWindowHandles = Sets.newHashSet("mainHandle");
        Mockito.when(mockWebDriver.getWindowHandles()).thenReturn(allWindowHandles);
        WebDriver.TargetLocator targetLocator = Mockito.mock(WebDriver.TargetLocator.class);
        Mockito.when(mockWebDriver.switchTo()).thenReturn(targetLocator);
        Mockito.when(targetLocator.window("mainHandle")).thenReturn(mockWebDriver);

        helper.closeAdditionalTabs(mockWebDriver, "mainHandle");

        Mockito.verify(mockWebDriver, Mockito.times(1)).getWindowHandles();
        Mockito.verify(targetLocator, Mockito.times(1)).window("mainHandle");
        Mockito.verify(targetLocator.window("mainHandle"), Mockito.times(1)).switchTo();
        Mockito.verify(mockWebDriver.switchTo().window("mainHandle"), Mockito.times(0)).close();

    }

    @Test
    public void test_closeWindowHandles_twoWindows_callsCloseOnce_onOtherWindow() {
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        Set<String> allWindowHandles = Sets.newHashSet("mainHandle", "otherHandle");
        Mockito.when(mockWebDriver.getWindowHandles()).thenReturn(allWindowHandles);
        WebDriver.TargetLocator mainTargetLocator = Mockito.mock(WebDriver.TargetLocator.class);
        Mockito.when(mockWebDriver.switchTo()).thenReturn(mainTargetLocator);

        // Need to define separate return web drivers to prevent mixing window() and close() calls in verify
        WebDriver mainHandleWebDriver = Mockito.mock(WebDriver.class);
        Mockito.when(mainTargetLocator.window("mainHandle")).thenReturn(mainHandleWebDriver);

        WebDriver otherHandleWebDriver = Mockito.mock(WebDriver.class);
        Mockito.when(mainTargetLocator.window("otherHandle")).thenReturn(otherHandleWebDriver);


        helper.closeAdditionalTabs(mockWebDriver, "mainHandle");

        Mockito.verify(mockWebDriver, Mockito.times(1)).getWindowHandles();
        Mockito.verify(mockWebDriver.switchTo(), Mockito.times(1)).window("mainHandle");
        Mockito.verify(mockWebDriver.switchTo().window("mainHandle"), Mockito.times(0)).close();
        Mockito.verify(mockWebDriver.switchTo().window("otherHandle"), Mockito.times(1)).close();
    }

    @Test
    public void test_closeWindowHandles_threeWindows_callsCloseTwice_onOtherWindows() {
        WebDriver mockWebDriver = Mockito.mock(WebDriver.class);
        Set<String> allWindowHandles = Sets.newHashSet("mainHandle", "otherHandle", "otherHandle2");
        Mockito.when(mockWebDriver.getWindowHandles()).thenReturn(allWindowHandles);
        WebDriver.TargetLocator mainTargetLocator = Mockito.mock(WebDriver.TargetLocator.class);
        Mockito.when(mockWebDriver.switchTo()).thenReturn(mainTargetLocator);

        // Need to define separate return web drivers to prevent mixing window() and close() calls in verify
        WebDriver mainHandleWebDriver = Mockito.mock(WebDriver.class);
        Mockito.when(mainTargetLocator.window("mainHandle")).thenReturn(mainHandleWebDriver);

        WebDriver otherHandleWebDriver = Mockito.mock(WebDriver.class);
        Mockito.when(mainTargetLocator.window("otherHandle")).thenReturn(otherHandleWebDriver);

        WebDriver otherHandle2WebDriver = Mockito.mock(WebDriver.class);
        Mockito.when(mainTargetLocator.window("otherHandle2")).thenReturn(otherHandle2WebDriver);


        helper.closeAdditionalTabs(mockWebDriver, "mainHandle");

        Mockito.verify(mockWebDriver, Mockito.times(1)).getWindowHandles();
        Mockito.verify(mockWebDriver.switchTo(), Mockito.times(1)).window("mainHandle");
        Mockito.verify(mockWebDriver.switchTo().window("mainHandle"), Mockito.times(0)).close();
        Mockito.verify(mockWebDriver.switchTo().window("otherHandle"), Mockito.times(1)).close();
        Mockito.verify(mockWebDriver.switchTo().window("otherHandle2"), Mockito.times(1)).close();
    }

    @Test
    public void test_sleepForTime_1000ms_takesAtLeast1000ms() {
        Long startTime = System.nanoTime();

        helper.sleepForTime(1000L);

        Long elapsedTime = System.nanoTime() - startTime;

        softly.assertThat(elapsedTime).isGreaterThan(1000000000L);

    }
}
