package com.arietis.bluetree.fleetmgr.rdl.service;

import com.arietis.bluetree.fleetmgr.rdl.config.Configuration;
import com.arietis.bluetree.fleetmgr.rdl.entity.RDLDownload;
import com.arietis.bluetree.fleetmgr.rdl.utility.Constants;
import com.arietis.bluetree.fleetmgr.rdl.utility.Helper;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DownloadServiceTest {

    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();
    private DownloadService downloadService;
    private WebDriver webDriver;
    private RestTemplate restTemplate;
    private Helper helper;

    @Before
    public void setup() {
        webDriver = Mockito.mock(FirefoxDriver.class);
        Configuration config = Mockito.spy(Configuration.class);
        restTemplate = Mockito.mock(RestTemplate.class);
        helper = Mockito.mock(Helper.class);
        downloadService = new DownloadService(config, restTemplate, helper);
        Mockito.when(config.getDownloadDirectory()).thenReturn("downloads");
        Mockito.when(webDriver.manage()).thenReturn(Mockito.mock(WebDriver.Options.class));
    }

    @Test
    public void test_downloadFile_dddSuccess() throws Exception {
        // Setup Mocking for Webdriver, filepath, callback captor
        String mockSessionID = UUID.randomUUID().toString();
        Mockito.when(webDriver.manage().getCookieNamed(Constants.ASP_NET_COOKIE)).thenReturn(Mockito.mock(Cookie.class));
        Mockito.when(webDriver.manage().getCookieNamed(Constants.ASP_NET_COOKIE).getValue()).thenReturn(mockSessionID);
        File downloadedFile = Mockito.mock(File.class);
        Path downloadPath = Paths.get("test.ddd");
        ArgumentCaptor<RequestCallback> requestCallbackArgumentCaptor = ArgumentCaptor.forClass(RequestCallback.class);

        Mockito.when(downloadedFile.toPath()).thenReturn(downloadPath);
        Mockito.when(restTemplate.execute(Mockito.any(String.class), Mockito.eq(HttpMethod.GET), Mockito.any(RequestCallback.class), Mockito.any(ResponseExtractor.class))).thenReturn(downloadedFile);
        RDLDownload rdlDownload = new RDLDownload("test.dlc", "test.ddd", "clcALC", "type", "test.dlc", new Date(), new Date(), new Date(), "from", "how", false, null);

        Boolean status = downloadService.downloadFile(webDriver, rdlDownload);

        // Verify Basic Download Calls work as expected
        softly.assertThat(status).isTrue();
        Mockito.verify(restTemplate, Mockito.times(1)).execute(Mockito.eq("test.ddd"), Mockito.eq(HttpMethod.GET), requestCallbackArgumentCaptor.capture(), Mockito.any(ResponseExtractor.class));
        Mockito.verify(helper, Mockito.times(1)).moveFile(Paths.get("test.ddd"), Paths.get("downloads/test.ddd"));

        // Verify SessionID is being added as cookie to download request
        RequestCallback requestCallback = requestCallbackArgumentCaptor.getValue();
        ClientHttpRequest clientHttpRequest = Mockito.mock(ClientHttpRequest.class);
        HttpHeaders httpHeaders = Mockito.mock(HttpHeaders.class);
        Mockito.when(clientHttpRequest.getHeaders()).thenReturn(httpHeaders);
        requestCallback.doWithRequest(clientHttpRequest);
        Mockito.verify(clientHttpRequest, Mockito.times(1)).getHeaders();
        Mockito.verify(httpHeaders, Mockito.times(1)).add("Cookie", Constants.ASP_NET_COOKIE + "=" + mockSessionID);
    }
}
